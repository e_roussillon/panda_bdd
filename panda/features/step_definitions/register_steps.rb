######### GIVEN #########

# CHECK base_steps.rb

######### WHEN #########

When(/^The user clicks on button '(Gender)' as '(male|femal)'$/) do | gender, type |

	case type
		when 'male'
			@page.click_gender_male
		when 'femal'
			@page.click_gender_femal
	end
end

# When(/^The user clicks on the field 'Pseudo'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user writes 'Dovi' on the field 'Pseudo'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user clicks on button 'Create'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user writes '<field_password' into the field 'Password'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user writes 'dovi@gmail\.com' into the field 'Email'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user  clicks on the field 'Email'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user writes '<field_email' into the field 'Email'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user writes '<field_pseudo' into the field 'Pseudo'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^the user writes '<field_email' into the field 'Email'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^the user clicks on the field 'Password'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^the user writes '<field_password'> into the field 'Password'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^the user clicks on button 'Create'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user writes '(\d+)' into the field 'Password'$/) do |arg1|
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user writes '<field_pseudo>' into the field 'Pseudo'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user writes 'dovi' into the field 'Pseudo'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user clicks on the fiels 'Pseudo'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user clicks on the filed 'Email'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user is clicks on button 'Create'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user writes 'dov' into the field 'Pseudo'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user clicks on field 'Gender'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user writes 'azertyuiopmlkjhgfdsqwxcvbn(\d+)' into the field 'Pseudo'$/) do |arg1|
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user writes '(\d+)' into the field 'Pseudo'$/) do |arg1|
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user writes '%£\*\^\$`\/' into the field 'Pseudo'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user clicks on the filed 'Pseudo'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# When(/^The user writes 'dovi@s' into the field 'Email'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end


######### THEN #########

Then(/^The fields '(Gender)' need to light up in '(Red)'$/) do  | filed, color |
  fail "The field 'Gender' waiting the the message" unless @page.check_error_gender?
end

# Then(/^The field 'Pseudo' need to be fillout with an error '<message_pseudo>'$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end

# Then(/^The screen 'Home' need to be on screen$/) do
#   pending # Write code here that turns the phrase above into concrete actions
# end