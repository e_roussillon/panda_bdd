######### GIVEN #########
Given(/^The user is on the screen '(Login|Register)'$/) do | screen_name |
  case screen_name
    when 'Login'
      @page = LoginScreen.new(5)
    when 'Register'
      @page = RegisterScreen.new(5)
    else
      fail "You forgot to set the action"
  end
end

######### WHEN #########

When(/^The user clicks on the field '(Pseudo|Email|Password|Birthday)'$/) do | field_name |
  case field_name
    when 'Pseudo'
      @page.click_pseudo
    when 'Email'
      @page.click_email
    when 'Password'
      @page.click_password
    when 'Birthday'
      @page.click_birthday
    else
      fail "You forgot to set the action"
  end
end

When(/^The user writes '([^\"]*)' into the field '(Pseudo|Email|Password|Birthday)'$/) do | string, field_name |
  
  case field_name
     when 'Pseudo'
      @page.edit_pseudo(string)
    when 'Email'
      @page.edit_email(string)
    when 'Password'
      @page.edit_password(string)
     when 'Birthday'
      @page.edit_birthday(string)
    else
      fail "You forgot to set the action"
  end

end

When(/^The user can not clicks on button '(Next|Pres)'$/) do | screen_name |

  case screen_name
    when 'Next'
      @page.can_not_click_toolbar_next
    when 'Pres'
      @page.can_not_click_toolbar_pres
    else
      fail "You forgot to set the action"
  end

end

When(/^The user clicks on button '(Done|Next|Pres|Login|Register|Facebook|Google|OK|Create)'$/) do | screen_name |
 
  case screen_name
    when 'Done'
      @page.click_toolbar_done
    when 'Next'
      @page.click_toolbar_next
    when 'Pres'
      @page.click_toolbar_pres
    when 'Login'
      @page.click_login
    when 'Facebook'
      @page.click_facebook
    when 'Google'
      @page.click_google
    when 'Register'
      @page.click_register
    when 'OK'
      fail "The Alert do not have the right button" unless @page.check_alert_message?(screen_name)
    when 'Create'
      @page.click_create
    else
      fail "You forgot to set the action"
  end

end

######### THEN #########

Then(/^The field '(Pseudo|Password|Email|Birthday)' need to be fillout with an error '([^\"]*)'$/) do  | field_name, message |
  case field_name
    when 'Pseudo'
      fail "The field 'Pseudo' waiting the the message" unless @page.check_error_pseudo?(message)
    when 'Email'
      fail "The field 'Email' waiting the the message" unless @page.check_error_email?(message)
    when 'Password'
      fail "The field 'Password' waiting the the message" unless @page.check_error_password?(message)
    when 'Birthday'
      fail "The field 'Birthday' waiting the the message" unless @page.check_error_birthday?(message)
    else
      fail "You forgot to set the action"
  end
end

Then(/^The '(Alert)' need to be fillout with an error '([^\"]*)'$/) do | field_name, message |

  case field_name
    when 'Alert'
      fail "The Alert do not have the right message" unless @page.check_alert_message?(message)
    else
      fail "You forgot to set the action"
  end

end

Then(/^The screen '(Facebook|Google|Register|Home)' need to be on screen$/) do | screen_name |

  case screen_name
    when 'Facebook'
      pending # express the regexp above with the code you wish you had
    when 'Google'
      pending # express the regexp above with the code you wish you had
    when 'Register'
      fail "You are not on register screen" unless @page = RegisterScreen.new(10)
    when 'Home'
      @page = HomeScreen.new(10)
    else
      fail "You forgot to set the action"
  end

end

Then(/^We will logout the user$/) do
  @page.logout_user
end