######### GIVEN #########

######### WHEN #########

When(/^We will set the mode '(Without Gender|Without Email|Without Pseudo|Without Birthday|OK)'$/) do | type |

	case type
		when 'Without Gender'
			@page.social_mode(1)
		when 'Without Email'
			@page.social_mode(2)
		when 'Without Pseudo'
			@page.social_mode(3)
		when 'Without Birthday'
			@page.social_mode(4)
		when 'OK'
			@page.social_mode(0)
	end
end

######### THEN #########