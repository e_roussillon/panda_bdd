require 'calabash-cucumber/ibase'

class IOSScreenBase < Calabash::IBase
  def self.element(element_name, &block)
    define_method(element_name.to_s, *block)
  end

  def initialize(await=0)
    super("")

    if await > 0
      await(timeout: await)
    end
 
  end

  class << self
    alias_method :value, :element
    alias_method :action, :element
    alias_method :trait, :element
  end

   element(:loading_screen)            { 'LOADING' }
   element(:button_logout)             { 'BUTTON_LOGOUT' }
   element(:button_left_pager)             { 'BUTTON_LEFT_PAGER' }
   element(:button_right_pager)             { 'BUTTON_RIGHT_PAGER' }

  # The progress bar of the application is a custom view
  def wait_for_progress
    sleep(2)
    wait_for(timeout: 10) { element_does_not_exist "* marked:'#{loading_screen}'" }
  end

  def has_text?(text)
    !query("* {text CONTAINS[c] '#{text}'}").empty? ||
      !query("* {accessibilityLabel CONTAINS[c] '#{text}'}").empty?
  end

  def drag_to(direction, element = nil)
    element = 'tableView' if element.nil?
    scroll(element, direction)
    sleep(1)
  end

  # In the iOS, an element could be found from its text or its accessibilityLabel
  # so this function looks for these two properties on the screen. When the query
  # looks for just a part of the text (CONTAINS[c]) then we need to specify if
  # we will look in accessibilityLabel or in any other propertie (marked)
  def ios_element_exists?(query)
    second_query = nil

    if query.include? 'CONTAINS[c]'
      if query.include? 'marked'
        second_query = query.gsub('marked', 'accessibilityLabel')
      end
      if query.include? 'accessibilityLabel'
        second_query = query.gsub('accessibilityLabel', 'marked')
      end
    end

    if second_query.nil?
      return element_exists(query)
    else
      element_exists(query) || element_exists(second_query)
    end
  end

  def drag_until_element_is_visible_with_special_query(direction, element)
    drag_until_element_is_visible direction, element,
                                  "* {accessibilityLabel CONTAINS[c] '#{element}'}"
  end

  def drag_until_element_is_visible(direction, element, query = nil, limit = 15)
    i = 0

    query = "* marked:'#{element}'" if query.nil?

    sleep(1)
    while !ios_element_exists?(query) && i < limit
      drag_to direction
      i += 1
    end

    fail "Executed #{limit} moviments #{direction} and the "\
         "element '#{element}' was not found on this view!" unless i < limit
  end

  def drag_for_specified_number_of_times(direction, times)
    times.times do
      drag_to direction
    end
  end

  # Negation indicates that we want a page that doesn't has
  # the message passed as parameter
  def is_on_page?(page_text, negation = '')
    should_not_have_exception = false
    should_have_exception = false
    begin
      wait_for(timeout: 5) { has_text? page_text }
      # If negation is not nil, we should raise an error
      # if this message was found on the view
      should_not_have_exception = true unless negation == ''
    rescue
      # only raise exception if negation is nil,
      # otherwise this is the expected behaviour
      should_have_exception = true if negation == ''
    end

    fail "Unexpected Page. The page should not have: '#{page_text}'" if
      should_not_have_exception

    fail "Unexpected Page. Expected was: '#{page_text}'" if
      should_have_exception
  end

  def enter(text, element, query = nil)
    query = "* marked:'#{element}'" if query.nil?

    begin
      wait_for(timeout: 5) { element_exists query }
    rescue
      # Just a better exception message
      raise "Element '#{element}' not found on view!"
    end

    touch query
    # Waits up to 20 seconds for the keyboard to show up
    begin
      wait_for(timeout: 10) { element_exists("view:'UIKeyboardAutomatic'") }
    rescue
      # If the keyboard didn't show up, tries another time
      # before rainsing the error message
      touch query
      wait_for(timeout: 5) { element_exists("view:'UIKeyboardAutomatic'") }
    end

    keyboard_enter_text text
  end

  def touch_screen_element(element, query = nil)
    query = "* marked:'#{element}'" if query.nil?
    begin
      wait_for(timeout: 5) { element_exists(query) }
      touch query
    rescue => e
      raise "Problem in touch screen element: '#{element}'\nError Message: #{e.message}"
    end
  end

  def touch_element_by_index(id, index)
    query = "* marked:'#{id}' index:#{index}"
    wait_for(timeout: 5) { element_exists(query) }
    touch(query)
  end

  def clear_text_field(field)
    clear_text("textField marked:'#{field}'")
  end

  def picker_select_date(date_string)
    date = Date.strptime(date_string, "%d/%m/%Y")
    query("view:'_UIDatePickerView'", setDate: date.strftime("%FT%T%:z"))

    sleep(0.4)
    # the query does not create a UIControlEventValueChanged event, so we have to do a touch event
    picker_scroll_up_on_column 0
    sleep(0.4)
    picker_scroll_down_on_column 0
    sleep(0.4)
  end

  def picker_scroll_up_on_column(column)
    new_row = picker_next_index_for_column column
    query("pickerTableView index:'#{column}'", [{selectRow:new_row}, {animated:1}, {notify:1}])
  end

  def picker_scroll_down_on_column(column)
    new_row = previous_index_for_column column
    #scroll_to_row("pickerTableView index:#{column}", new_row)
    query("pickerTableView index:'#{column}'", [{:selectRow => new_row}, {:animated => 1}, {:notify => 1}])
  end

  def picker_next_index_for_column (column)
    picker_current_index_for_column(column) + 1
  end

  def picker_current_index_for_column (column)
    arr = query('pickerTableView', :selectionBarRow)
    arr[column]
  end

  def previous_index_for_column (column)
    picker_current_index_for_column(column) - 1
  end

  def touch_picker_item_by_index(index)
    label = query('pickerView', :delegate,
                  [{ pickerView: nil }, { titleForRow: index },
                   { forComponent: 0 }])

    # Exception if element no found
    fail "Picker item index #{index} not found." if label.nil?
    # Label is an array of 1 element. Just picking the first.
    label = label.first

    # Touching the first item using it's text and Javascript function
    uia(%[uia.selectPickerValues('{0 "#{label}"}')])

    # Touching the OK button to close the Picker
    touch "* marked:'OK'"
  end

  #TOOL BAR - Keyboard
  def click_toolbar_done
    touch("UIToolbarButton index:2")
    await(timeout: 1)
  end

  def click_toolbar_next
    touch("UIToolbarButton index:1")
    await(timeout: 1)
  end

  def click_toolbar_pres
    touch("UIToolbarButton index:0")
    await(timeout: 1)
  end

  def can_not_click_toolbar_next
    await(timeout: 1)
    query("UIToolbarButton index:1", :isEnabled)[0] == 0
  end

  def can_not_click_toolbar_pres
    await(timeout: 1)
    query("UIToolbarButton index:0", :isEnabled)[0] == 0
  end

  #CHECK ERROR MESSAGE INTO FIELD
  def check_label_error?(field, message)
    await(timeout: 1)
    query("* marked:'#{field}'", :labelError, :text)[0] == message
  end

  # Alert
  def check_alert_message?(message)
    await(timeout: 1)
    touch("UILabel marked:'#{message}'")
  end

  # Logout User
  def logout_user
    index = -1

    loop do 
      await(timeout: 2)
      index = index + 1

      if element_exists("* id:'#{button_logout}'")
        touch("* id:'#{button_logout}'")
        index = 3
      else
        touch("* id:'#{button_left_pager}'")
        await(timeout: 5)
      end

      break if index > 2
    end 

    
    
  end

end
