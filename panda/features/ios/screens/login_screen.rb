class LoginScreen < IOSScreenBase
  # The screen identificator
  trait(:trait)                 { "* marked:'#{layout_name}'" }

  # Declare all the elements of this screen
  element(:layout_name)          	{ 'LOGIN_SCREEN' }
  element(:field_email)          	{ 'FIELD_EMAIL' }
  element(:field_pass)          	{ 'FIELD_PASSWORD' }
  element(:button_login)         	{ 'BUTTON_LOGIN' }
  element(:button_register)         { 'BUTTON_REGISTER' }
  element(:button_facebook)         { 'BUTTON_FACEBOOK' }
  element(:button_google)         	{ 'BUTTON_GOOGLE' }
  element(:layout_register)         { 'REGISTER_SCREEN' }
  element(:toolbar_done)         { 'TOOLBAR_DONE' }
  element(:toolbar_next)         { 'TOOLBAR_NEXT' }
  element(:toolbar_pres)         { 'TOOLBAR_PRES' }
  
  	# Edit
  	def edit_email(email) 
  		enter email, field_email
  		await(timeout: 1)
  	end

  	def edit_password(password) 
  		enter password, field_pass
  		await(timeout: 1)
  	end

  	# On Screen

  	def on_screen_register?
  		element_exists "* marked:'#{layout_register}'"
  	end

    # Facebook mode
    def social_mode(mode)
      query("* id:'#{button_facebook}'", setSocialCalMode:mode)
    end


    # Check Message
    def check_error_email?(msg)
      # 'Field is empty or invalid'
      # check_label_error?('FIELD_EMAIL', msg)
      check_label_error?(field_email, msg)
    end

    def check_error_password?(msg)
      # 'Field is empty or invalid'
      # check_label_error?('FIELD_PASSWORD', msg)
      check_label_error?(field_pass, msg)
    end
    
  	# Click
    def click_return
      keyboard_enter_char "Return"
      await(timeout: 1)
    end

  	def click_email
  		touch_screen_element field_email
  		await(timeout: 1)
  	end

    def click_password
      touch_screen_element field_pass
      await(timeout: 1)
    end

  	def click_login
  		touch_screen_element button_login
  	end

  	def click_register
  		touch_screen_element button_register
  	end

  	def click_facebook
  		#http://stackoverflow.com/questions/29766317/how-to-change-default-login-button-in-fbsdkloginbutton-in-ios
  		touch_screen_element button_facebook
  	end

  	def click_google
  		#https://medium.com/swift-programming/google-in-swift-a0943352dfe9
  		#backend
  		#https://developers.google.com/identity/sign-in/ios/backend-auth
  		touch_screen_element button_google
  	end

end
