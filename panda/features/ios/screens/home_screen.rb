class HomeScreen < IOSScreenBase
  # The screen identificator
  trait(:trait)                 { "* marked:'#{layout_name}'" }

  # Declare all the elements of this screen
  element(:layout_name)          	{ 'HOME_SCREEN' }
  
end
