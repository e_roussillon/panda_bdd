class RegisterScreen < IOSScreenBase
  # The screen identificator
  trait(:trait)                 { "* marked:'#{layout_name}'" }

  # Declare all the elements of this screen
  element(:layout_name)          	{ 'REGISTER_SCREEN' }
  element(:button_male)          	{ 'BUTTON_MALE' }
  element(:button_femal)          	{ 'BUTTON_FEMAL' }
  element(:field_email)          	{ 'FIELD_EMAIL' }
  element(:field_pass)          	{ 'FIELD_PASSWORD' }
  element(:field_pseudo)          	{ 'FIELD_PSEUDO' }
  element(:field_birthday)          { 'FIELD_BIRTHDAY' }
  element(:date_picker)				{ 'DATE_PICKER'}
  element(:button_create)         	{ 'BUTTON_CREATE' }
  element(:layout_home)         	{ 'GENERAL_SCREEN' }
  
	# Edit
	def edit_pseudo(pseudo) 
		enter pseudo, field_pseudo
		await(timeout: 1)
	end

	def edit_email(email) 
		enter email, field_email
		await(timeout: 1)
	end

	def edit_password(password) 
		enter password, field_pass
		await(timeout: 1)
	end

	def edit_birthday(birthday) 
		picker_select_date(birthday)
		await(timeout: 1)
	end

	# Check Message
	def check_error_email?(msg)
		check_label_error?(field_email, msg)
	end

	def check_error_password?(msg)
		check_label_error?(field_pass, msg)
	end

	def check_error_pseudo?(msg)
		check_label_error?(field_pseudo, msg)
	end

	def check_error_birthday?(msg)
		check_label_error?(field_birthday, msg)
	end

	def check_error_gender?
		query("* marked:'#{button_male}'", :isErrorShowed)[0] == 1 && query("* marked:'#{button_femal}'", :isErrorShowed)[0] == 1
	end

	# Click
	def click_return
		keyboard_enter_char "Return"
		await(timeout: 1)
	end

	def click_gender_male
		touch_screen_element button_male
		await(timeout: 1)
	end

	def click_gender_femal
		touch_screen_element button_femal
		await(timeout: 1)
	end

	def click_pseudo
		touch_screen_element field_pseudo
		await(timeout: 1)
	end

	def click_email
		touch_screen_element field_email
		await(timeout: 1)
	end

	def click_password
		touch_screen_element field_pass
		await(timeout: 1)
	end

	def click_birthday
		touch_screen_element field_birthday
		await(timeout: 1)
	end

	def click_create
		touch_screen_element button_create
	end
end