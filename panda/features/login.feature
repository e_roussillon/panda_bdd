# language: en
Feature: Login 

      This features will test all the screen login:
            - Test when the user do not fillout all or some fields
            - Test the login with Facebook and Google
            - Test a success Login

Scenario: Setup the DB for the feature
      Given We will clean tabe user with relation
      And We will insert a default user


Scenario: We will not fillout any fields and click on 'Register'
       Given The user is on the screen 'Login'
       When The user clicks on button 'Register'
       Then The screen 'Register' need to be on screen


Scenario: We will not fillout any fields and click on 'Facebook', the field 'Gender' will be missing 
       Given The user is on the screen 'Login'
       When We will set the mode 'Without Gender'
       And The user clicks on button 'Facebook'
       Then The screen 'Register' need to be on screen
       And The fields 'Gender' need to light up in 'Red'


Scenario Outline: We will not fillout any fields and click on 'Facebook', the field 'Email' will be missing 
       Given The user is on the screen 'Login'
       When We will set the mode 'Without Email'
       And The user clicks on button 'Facebook'
       Then The screen 'Register' need to be on screen
       And The field 'Email' need to be fillout with an error '<email_message>'

Examples:
      | email_message             |
      | Field is empty or invalid |


Scenario Outline: We will not fillout any fields and click on 'Facebook', the field 'Pseudo' will be missing 
       Given The user is on the screen 'Login'
       When We will set the mode 'Without Pseudo'
       And The user clicks on button 'Facebook'
       Then The screen 'Register' need to be on screen
       And The field 'Pseudo' need to be fillout with an error '<pseudo_message>'

Examples:
      | pseudo_message                                    |
      | Your speudo have to be between 4 to 30 characters |


Scenario Outline: We will not fillout any fields and click on 'Facebook', the field 'Birthday' will be missing 
       Given The user is on the screen 'Login'
       When We will set the mode 'Without Birthday'
       And The user clicks on button 'Facebook'
       Then The screen 'Register' need to be on screen
       And The field 'Birthday' need to be fillout with an error '<birthday_message>'

Examples:
      | birthday_message          |
      | Field is empty or invalid |


Scenario: We will not fillout any fields and click on 'Facebook' with success
      Given The user is on the screen 'Login'
      When We will set the mode 'OK'
      And The user clicks on button 'Facebook'
      Then The screen 'Home' need to be on screen
      And We will logout the user


Scenario Outline: We will not fillout any fields and click on 'Login'
       Given The user is on the screen 'Login'
       When The user clicks on button 'Login'
       Then The field 'Email' need to be fillout with an error '<email_message>'
       And The field 'Password' need to be fillout with an error '<password_message>'

Examples:
      | email_message             | password_message          |
      | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Email' and click on 'Login'
       Given The user is on the screen 'Login'
       When The user clicks on the field 'Email'
       And The user writes '<field_email>' into the field 'Email' 
       And The user clicks on button 'Done'
       And The user clicks on button 'Login'
       Then The field 'Password' need to be fillout with an error '<password_message>'

Examples:
      | field_email    | password_message          |
      | dovi@gmail.com | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Password' and click on 'Login'
       Given The user is on the screen 'Login'
       When The user clicks on the field 'Password'
       And The user writes '<field_password>' into the field 'Password' 
       And The user clicks on button 'Done'
       And The user clicks on button 'Login'
       Then The field 'Email' need to be fillout with an error '<email_message>'

Examples:
      | field_password | email_message             |
      | 123456         | Field is empty or invalid |


Scenario Outline: We will fillout the fields 'Email' and 'Password' but the email will be invalid
       Given The user is on the screen 'Login'
       When The user clicks on the field 'Email'
       And The user writes '<field_email>' into the field 'Email' 
       And The user clicks on button 'Done'
       And The user clicks on the field 'Password'
       And The user writes '<field_password>' into the field 'Password' 
       And The user clicks on button 'Done'
       And The user clicks on button 'Login'
       Then The field 'Email' need to be fillout with an error '<email_message>'

Examples:
      | field_email    | field_password | email_message             |
      | dovi@          | 123456         | Field is empty or invalid |

Scenario Outline: We will fillout the fields 'Email' and 'Password' but the email or password do not exit
       Given The user is on the screen 'Login'
       When The user clicks on the field 'Email'
       And The user writes '<field_email>' into the field 'Email' 
       And The user clicks on button 'Done'
       And The user clicks on the field 'Password'
       And The user writes '<field_password>' into the field 'Password' 
       And The user clicks on button 'Done'
       And The user clicks on button 'Login'
       Then The 'Alert' need to be fillout with an error '<alert_message>'
       And The user clicks on button 'OK'

Examples:
      | field_email    | field_password | alert_message                              |
      | dovi@gmail.com | 12345612       | Please check again your email or password! |
      | dovi@gma.com   | 123456         | Please check again your email or password! |


Scenario Outline: We will check if the toolbar next, pres and done
       Given The user is on the screen 'Login'
       When The user clicks on the field 'Email'
       And The user clicks on button 'Next'
       And The user writes '<field_password>' into the field 'Password'
       And The user can not clicks on button 'Next'
       And The user clicks on button 'Pres'
       And The user writes '<field_email>' into the field 'Email'
       And The user can not clicks on button 'Pres'
       Then The user clicks on button 'Done'

Examples:
      | field_email    | field_password | 
      | dovi@gmail.com | 123            |


Scenario Outline: We will fillout the fields 'Email' and 'Password' and login successfully
      Given The user is on the screen 'Login'
      When The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Password'
      And The user writes '<field_password>' into the field 'Password'
      And The user clicks on button 'Done'
      And The user clicks on button 'Login'
      Then The screen 'Home' need to be on screen
      And We will logout the user

Examples:
      | field_email    | field_password |
      | dovi@gmail.com | 123456         |
