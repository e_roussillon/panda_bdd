# language: en
Feature: Register

      Before to be on the screen Register, The app will will launch an open the Login Screen. That this point
      we will need to click on the button Register.

      This features will test all the screen Register:
            - Test when the user do not fillout all or some fields
            - Test if the field do not respecte the sepc (less than 6 characters, only number, only special character)
            - Test a success Register

Background:
      Given The user is on the screen 'Login'
      And The user clicks on button 'Register'

Scenario: Setup the DB for the feature
      Given We will clean tabe user with relation


Scenario Outline: We will not fillout any fields and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on button 'Create'
      Then The fields 'Gender' need to light up in 'Red'
      And The field 'Pseudo' need to be fillout with an error '<pseudo_message>'
      And The field 'Email' need to be fillout with an error '<email_message>'
      And The field 'Birthday' need to be fillout with an error '<birthday_message>'
      And The field 'Password' need to be fillout with an error '<password_message>'
      
Examples:
      | pseudo_message            | password_message            | email_message             | birthday_message          |
      | Field is empty or invalid | Field is empty or invalid   | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Gender' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on button 'Create'
      Then The field 'Pseudo' need to be fillout with an error '<pseudo_message>'
      And The field 'Email' need to be fillout with an error '<email_message>'
      And The field 'Birthday' need to be fillout with an error '<birthday_message>'
      And The field 'Password' need to be fillout with an error '<password_message>'

Examples:
      | gender_type | pseudo_message              | password_message          | email_message             | birthday_message          |
      | male        | Field is empty or invalid   | Field is empty or invalid | Field is empty or invalid | Field is empty or invalid |
      | femal       | Field is empty or invalid   | Field is empty or invalid | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Pseudo' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on the field 'Pseudo'
      And The user writes '<field_pseudo>' into the field 'Pseudo'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The fields 'Gender' need to light up in 'Red'
      And The field 'Email' need to be fillout with an error '<email_message>'
      And The field 'Birthday' need to be fillout with an error '<birthday_message>'
      And The field 'Password' need to be fillout with an error '<password_message>'

  Examples:
      | field_pseudo  | password_message          | email_message             | birthday_message          |
      | Dovi          | Field is empty or invalid | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Email' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The fields 'Gender' need to light up in 'Red'
      And The field 'Pseudo' need to be fillout with an error '<pseudo_message>'
      And The field 'Birthday' need to be fillout with an error '<birthday_message>'
      And The field 'Password' need to be fillout with an error '<password_message>'

Examples:
      | field_email    | pseudo_message            | password_message          | birthday_message          |
      | dovi@gmail.com | Field is empty or invalid | Field is empty or invalid | Field is empty or invalid |
      

Scenario Outline: We will fillout only the field 'Birthday' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on the field 'Birthday'
      And The user writes '<field_birthday>' into the field 'Birthday'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The fields 'Gender' need to light up in 'Red'
      And The field 'Pseudo' need to be fillout with an error '<pseudo_message>'
      And The field 'Email' need to be fillout with an error '<email_message>'
      And The field 'Password' need to be fillout with an error '<password_message>'

Examples:
      | field_birthday | pseudo_message            | email_message             | password_message          |
      | 09/12/2000     | Field is empty or invalid | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Password' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on the field 'Password'
      And The user writes '<field_password>' into the field 'Password'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The fields 'Gender' need to light up in 'Red'
      And The field 'Pseudo' need to be fillout with an error '<pseudo_message>'
      And The field 'Email' need to be fillout with an error '<email_message>'
      And The field 'Birthday' need to be fillout with an error '<birthday_message>'

Examples:
      | field_password | pseudo_message            | email_message              | birthday_message          |
      | 123456          | Field is empty or invalid | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Gender' and 'Pseudo' click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on the field 'Pseudo'
      And The user writes '<field_pseudo>' into the field 'Pseudo'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The field 'Email' need to be fillout with an error '<email_message>'
      And The field 'Birthday' need to be fillout with an error '<birthday_message>'
      And The field 'Password' need to be fillout with an error '<password_message>'

Examples:
      | gender_type| field_pseudo | email_message             | password_message          | birthday_message          |
      | male       | dovi         | Field is empty or invalid | Field is empty or invalid | Field is empty or invalid |
      | femal      | dovi         | Field is empty or invalid | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Gender' and 'Email' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The field 'Pseudo' need to be fillout with an error '<pseudo_message>'
      And The field 'Birthday' need to be fillout with an error '<birthday_message>'
      And The field 'Password' need to be fillout with an error '<password_message>'

Examples:
      | field_email    | gender_type | pseudo_message            | password_message          | birthday_message          |
      | dovi@gmail.com | male        | Field is empty or invalid | Field is empty or invalid | Field is empty or invalid |
      | dovi@gmail.com | femal       | Field is empty or invalid | Field is empty or invalid | Field is empty or invalid |

Scenario Outline: We will fillout only the field 'Gender' and 'Birthday' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on the field 'Birthday'
      And The user writes '<field_birthday>' into the field 'Birthday'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The field 'Pseudo' need to be fillout with an error '<pseudo_message>'
      And The field 'Email' need to be fillout with an error '<email_message>'
      And The field 'Password' need to be fillout with an error '<password_message>'

Examples:
      | field_birthday | gender_type | pseudo_message            | password_message          | email_message          |
      | 10/10/2000     | male        | Field is empty or invalid | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Gender' and 'Password' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on the field 'Password'
      And The user writes '<field_password>' into the field 'Password'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The field 'Pseudo' need to be fillout with an error '<pseudo_message>'
      And The field 'Email' need to be fillout with an error '<email_message>'
      And The field 'Birthday' need to be fillout with an error '<birthday_message>'

Examples:
      | gender_type | field_password | email_message             | pseudo_message            | birthday_message          |
      | male        | 123456         | Field is empty or invalid | Field is empty or invalid | Field is empty or invalid |
      | femal       | 123456         | Field is empty or invalid | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Pseudo' and 'Email' click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on the field 'Pseudo'
      And The user writes '<field_pseudo>' into the field 'Pseudo' 
      And The user clicks on button 'Next'
      And The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The fields 'Gender' need to light up in 'Red'
      And The field 'Birthday' need to be fillout with an error '<birthday_message>'
      And The field 'Password' need to be fillout with an error '<password_message>'

Examples:
      | field_email    | field_pseudo | password_message          | birthday_message          |
      | dovi@gmail.com | dovi         | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Pseudo' and 'Birthday' click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on the field 'Pseudo'
      And The user writes '<field_pseudo>' into the field 'Pseudo' 
      And The user clicks on button 'Next'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Birthday'
      And The user writes '<field_birthday>' into the field 'Birthday'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The fields 'Gender' need to light up in 'Red'
      And The field 'Email' need to be fillout with an error '<email_message>'
      And The field 'Password' need to be fillout with an error '<password_message>'

Examples:
      | field_birthday | field_pseudo | password_message          | email_message             |
      | 10/10/2000     | dovi         | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Pseudo' and 'Password' click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on the field 'Pseudo'
      And The user writes '<field_pseudo>' into the field 'Pseudo'
      And The user clicks on button 'Next'
      And The user clicks on button 'Next'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Password'
      And The user writes '<field_password>' into the field 'Password'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The fields 'Gender' need to light up in 'Red'
      And The field 'Email' need to be fillout with an error '<email_message>'
      And The field 'Birthday' need to be fillout with an error '<birthday_message>'
      
Examples:
      | field_password| field_pseudo | email_message             | birthday_message          |
      | 123456        | dovi         | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Email' and 'Birthday' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Birthday'
      And The user writes '<field_birthday>' into the field 'Birthday'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The fields 'Gender' need to light up in 'Red'
      And The field 'Pseudo' need to be fillout with an error '<pseudo_message>'
      And The field 'Password' need to be fillout with an error '<password_message>'

Examples:
      | field_email    | field_birthday | pseudo_message            | password_message          |
      | dovi@gmail.com | 10/10/2000     | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Email' and 'Password' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Next'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Password'
      And The user writes '<field_password>' into the field 'Password'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The fields 'Gender' need to light up in 'Red'
      And The field 'Pseudo' need to be fillout with an error '<pseudo_message>'
      And The field 'Birthday' need to be fillout with an error '<birthday_message>'

Examples:
      | field_email    | field_password | pseudo_message            | birthday_message          |
      | dovi@gmail.com | 123456         | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Birthday' and 'Password' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on the field 'Birthday'
      And The user writes '<field_birthday>' into the field 'Birthday'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Password'
      And The user writes '<field_password>' into the field 'Password'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The fields 'Gender' need to light up in 'Red'
      And The field 'Pseudo' need to be fillout with an error '<pseudo_message>'
      And The field 'Email' need to be fillout with an error '<email_message>'

Examples:
      | field_birthday | field_password | pseudo_message            | email_message             |
      | 10/10/2000     | 123456         | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Gender' and 'Pseudo' and 'Email' click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on the field 'Pseudo'
      And The user writes '<field_pseudo>' into the field 'Pseudo'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The field 'Password' need to be fillout with an error '<password_message>'
      And The field 'Birthday' need to be fillout with an error '<birthday_message>'

Examples:
      | field_email     | field_pseudo | gender_type | password_message          | birthday_message          |
      | dovi@gmail.com  | dovi         | male        | Field is empty or invalid | Field is empty or invalid |
      | dovi@gmail.com  | dovi         | femal       | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Gender' and 'Pseudo' and 'Birthday' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on the field 'Pseudo'
      And The user writes '<field_pseudo>' into the field 'Pseudo'
      And The user clicks on button 'Next'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Birthday'
      And The user writes '<field_birthday>' into the field 'Birthday'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The field 'Email' need to be fillout with an error '<email_message>'
      And The field 'Password' need to be fillout with an error '<password_message>'

Examples:
      | field_birthday | field_pseudo | gender_type | email_message             | password_message          |
      | 10/10/2000     | dovi         | male        | Field is empty or invalid | Field is empty or invalid |
      | 10/10/2000     | dovi         | femal       | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Gender' and 'Pseudo' and 'Password' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on the field 'Pseudo'
      And The user writes '<field_pseudo>' into the field 'Pseudo'
      And The user clicks on button 'Next'
      And The user clicks on button 'Next'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Password'
      And The user writes '<field_password>' into the field 'Password'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The field 'Email' need to be fillout with an error '<email_message>'
      And The field 'Birthday' need to be fillout with an error '<birthday_message>'

Examples:
      | field_password | field_pseudo | gender_type | email_message             | birthday_message          |
      | 123456         | dovi         | male        | Field is empty or invalid | Field is empty or invalid |
      | 123456         | dovi         | femal       | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Pseudo' and 'Email' and 'Birthday' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on the field 'Pseudo'
      And The user writes '<field_pseudo>' into the field 'Pseudo'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Birthday'
      And The user writes '<field_birthday>' into the field 'Birthday'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The fields 'Gender' need to light up in 'Red'
      And The field 'Password' need to be fillout with an error '<password_message>'

Examples:
      | field_email     | field_pseudo | field_birthday | password_message          |
      | dovi@gmail.com  | dovi         | 10/10/2000     | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Pseudo' and 'Email' and 'Password' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on the field 'Pseudo'
      And The user writes '<field_pseudo>' into the field 'Pseudo'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Next'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Password'
      And The user writes '<field_password>' into the field 'Password'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The fields 'Gender' need to light up in 'Red'
      And The field 'Birthday' need to be fillout with an error '<birthday_message>'

Examples:
      | field_email     | field_pseudo | field_password | birthday_message          |
      | dovi@gmail.com  | dovi         | 123456         | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Gender' and 'Email' and 'Birthday' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Birthday'
      And The user writes '<field_birthday>' into the field 'Birthday'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The field 'Pseudo' need to be fillout with an error '<pseudo_message>'
      And The field 'Password' need to be fillout with an error '<password_message>'

Examples:
      | field_email     | gender_type | field_birthday | pseudo_message            | password_message          |
      | dovi@gmail.com  | male        | 10/10/2000     | Field is empty or invalid | Field is empty or invalid |
      | dovi@gmail.com  | femal       | 10/10/2000     | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Gender' and 'Email' and 'Password' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Next'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Password'
      And The user writes '<field_password>' into the field 'Password'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The field 'Pseudo' need to be fillout with an error '<pseudo_message>'
      And The field 'Birthday' need to be fillout with an error '<birthday_message>'

Examples:
      | field_email     | gender_type | field_password | birthday_message          | pseudo_message             |
      | dovi@gmail.com  | male        | 123456         | Field is empty or invalid | Field is empty or invalid  |
      | dovi@gmail.com  | femal       | 123456         | Field is empty or invalid | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Gender' and 'Pseudo' and 'Email' and 'Birthday' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on the field 'Pseudo'
      And The user writes '<field_pseudo>' into the field 'Pseudo'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Birthday'
      And The user writes '<field_birthday>' into the field 'Birthday'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The field 'Password' need to be fillout with an error '<password_message>'

Examples:
      | field_email     | gender_type | field_birthday | field_pseudo | password_message          |
      | dovi@gmail.com  | male        | 10/10/2000     | dovi         | Field is empty or invalid |
      | dovi@gmail.com  | femal       | 10/10/2000     | dovi         | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Gender' and 'Pseudo' and 'Email' and 'Password' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on the field 'Pseudo'
      And The user writes '<field_pseudo>' into the field 'Pseudo'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Next'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Password'
      And The user writes '<field_password>' into the field 'Password'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The field 'Birthday' need to be fillout with an error '<birthday_message>'

Examples:
      | field_email     | gender_type | field_password | field_pseudo | birthday_message          |
      | dovi@gmail.com  | male        | 123456         | dovi         | Field is empty or invalid |
      | dovi@gmail.com  | femal       | 123456         | dovi         | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Gender' and 'Pseudo' and 'Birthday' and 'Password' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on the field 'Pseudo'
      And The user writes '<field_pseudo>' into the field 'Pseudo'
      And The user clicks on button 'Next'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Birthday'
      And The user writes '<field_birthday>' into the field 'Birthday'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Password'
      And The user writes '<field_password>' into the field 'Password'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The field 'Email' need to be fillout with an error '<email_message>'

Examples:
      | field_birthday | gender_type | field_password | field_pseudo | email_message             |
      | 10/10/2000     | male        | 123456         | dovi         | Field is empty or invalid |
      | 10/10/2000     | femal       | 123456         | dovi         | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Gender' and 'Email' and 'Birthday' and 'Password' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Birthday'
      And The user writes '<field_birthday>' into the field 'Birthday'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Password'
      And The user writes '<field_password>' into the field 'Password'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The field 'Pseudo' need to be fillout with an error '<pseudo_message>'

Examples:
      | field_birthday | gender_type | field_password | field_email    | pseudo_message            |
      | 10/10/2000     | male        | 123456         | dovi@gmail.com | Field is empty or invalid |
      | 10/10/2000     | femal       | 123456         | dovi@gmail.com | Field is empty or invalid |


Scenario Outline: We will fillout only the field 'Pseudo' and 'Email' and 'Birthday' and 'Password' and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on the field 'Pseudo'
      And The user writes '<field_email>' into the field 'Pseudo'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Birthday'
      And The user writes '<field_birthday>' into the field 'Birthday'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Password'
      And The user writes '<field_password>' into the field 'Password'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The fields 'Gender' need to light up in 'Red'

Examples:
      | field_birthday | field_password | field_email   | pseudo_message            |
      | 10/10/2000     | 123456         | dovi@gmail.com | Field is empty or invalid |


Scenario Outline: We will fillout the field 'Gender', 'Pseudo', 'Email', 'Birthday' and 'Password' but the field pseudo will be less than 4 and over 30 and click on 'Create'
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on the field 'Pseudo'
      And The user writes '<field_pseudo>' into the field 'Pseudo'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Birthday'
      And The user writes '<field_birthday>' into the field 'Birthday'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Password'
      And The user writes '<field_password>' into the field 'Password'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The field 'Pseudo' need to be fillout with an error '<pseudo_message>'

Examples:
      | field_email     | gender_type | field_password | field_pseudo | field_birthday | pseudo_message                                  |
      | dovi@gmail.com  | male        | 123456         | dov          | 10/10/2000     | Your speudo have to be between 4 to 30 characters |
      | dovi@gmail.com  | femal       | 123456         | d    o       | 10/10/2000     | Your speudo have to be between 4 to 30 characters |
      | dovi@gmail.com  | male       | 123456          | azertyuiopmlkjhgfdsqwxcvbn12345          | 10/10/2000     | Your speudo have to be between 4 to 30 characters |
      | dovi@gmail.com  | femal       | 123456         | azertyuiopmlkjhgfdsqwxcvbn12345          | 10/10/2000     | Your speudo have to be between 4 to 30 characters |


Scenario Outline: We will fillout the field 'Gender', 'Pseudo', 'Email', 'Birthday' and 'Password' and the field 'Pseudo' will contains only number, then special character
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on the field 'Pseudo'
      And The user writes '<field_pseudo>' into the field 'Pseudo'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Birthday'
      And The user writes '<field_birthday>' into the field 'Birthday'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Password'
      And The user writes '<field_password>' into the field 'Password'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The field 'Pseudo' need to be fillout with an error '<pseudo_message>'

Examples:
      | field_email     | gender_type | field_password | field_pseudo | field_birthday | pseudo_message            |
      | dovi@gmail.com  | male        | 123456         | 123456       | 10/10/2000     | Your pseudo must be set with characters and numbers |
      | dovi@gmail.com  | femal       | 123456         | 123456       | 10/10/2000     | Your pseudo must be set with characters and numbers |
      | dovi@gmail.com  | male        | 123456         | %£*^$@#      | 10/10/2000     | Your pseudo must be set with characters and numbers |
      | dovi@gmail.com  | femal       | 123456         | %£*^$@#      | 10/10/2000     | Your pseudo must be set with characters and numbers |
      | dovi@gmail.com  | male        | 123456         | dov_ 12      | 10/10/2000     | Your pseudo must be set with characters and numbers |
      | dovi@gmail.com  | femal       | 123456         | dov_ 12      | 10/10/2000     | Your pseudo must be set with characters and numbers |
  

Scenario Outline: We will fillout the field 'Gender', 'Pseudo', 'Email', 'Birthday' and 'Password' and the field 'Email' will contains an invalid format 
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on the field 'Pseudo'
      And The user writes '<field_pseudo>' into the field 'Pseudo'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Birthday'
      And The user writes '<field_birthday>' into the field 'Birthday'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Password'
      And The user writes '<field_password>' into the field 'Password'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The field 'Email' need to be fillout with an error '<email_message>'

Examples:
      | field_email | gender_type | field_password | field_pseudo | field_birthday | email_message           |
      | dovi@g      | male        | 123456         | dovi         | 10/10/2000     | Your email is not valid |
      | dovi@g      | femal       | 123456         | dovi         | 10/10/2000     | Your email is not valid |


Scenario Outline: We will fillout the field 'Gender', 'Pseudo', 'Email', 'Birthday' and 'Password' and the field 'Password' size will be less than 6 
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on the field 'Pseudo'
      And The user writes '<field_pseudo>' into the field 'Pseudo'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Birthday'
      And The user writes '<field_birthday>' into the field 'Birthday'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Password'
      And The user writes '<field_password>' into the field 'Password'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The field 'Password' need to be fillout with an error '<password_message>'

Examples:
      | field_email    | gender_type | field_password | field_pseudo | field_birthday | password_message          |
      | dovi@gmail.com | male        | 1234           | dovi         | 10/10/2000     | Your password have to be set with minimum 6 characters |
      | dovi@gmail.com | femal       | 1234           | dovi         | 10/10/2000     | Your password have to be set with minimum 6 characters |

Scenario Outline: We will fillout the field 'Gender', 'Pseudo', 'Email', 'Birthday' and 'Password' click on 'Create' with success
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on the field 'Pseudo'
      And The user writes '<field_pseudo>' into the field 'Pseudo'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Birthday'
      And The user writes '<field_birthday>' into the field 'Birthday'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Password'
      And The user writes '<field_password>' into the field 'Password'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The screen 'Home' need to be on screen
      And We will logout the user

Examples:
      | field_email     | gender_type | field_password | field_pseudo | field_birthday |
      | dovi@gmail.com  | male        | 123456         | dovi         | 10/10/2000     |


Scenario Outline: We will fillout the field 'Gender', 'Pseudo', 'Email', 'Birthday' and 'Password' click on 'Create' with fail, the email is already use
      Given The user is on the screen 'Register'
      When The user clicks on button 'Gender' as '<gender_type>'
      And The user clicks on the field 'Pseudo'
      And The user writes '<field_pseudo>' into the field 'Pseudo'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Email'
      And The user writes '<field_email>' into the field 'Email'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Birthday'
      And The user writes '<field_birthday>' into the field 'Birthday'
      And The user clicks on button 'Next'
      And The user clicks on the field 'Password'
      And The user writes '<field_password>' into the field 'Password'
      And The user clicks on button 'Done'
      And The user clicks on button 'Create'
      Then The field 'Email' need to be fillout with an error '<email_message>'

Examples:
      | field_email     | gender_type | field_password | field_pseudo | field_birthday | email_message           |
      | dovi@gmail.com  | male        | 123456         | dovi         | 10/10/2000     | This email is already used |