# require 'singleton'
require 'mysql2'

class DB_UTIL
   # include Singleton
   
	def initialize()
		@client = Mysql2::Client.new(:host => "localhost", :port => 8889, :username => "root", :password => "root", :database => "pandaDB")  	
	end

	def query(sql)
		# puts ""
		# puts "RUN --> SQL :"
		# puts sql
		@client.query(sql)
	end

	def last_id()
		@client.last_id
	end

	def result()
		@client.affected_rows
	end

	def close()
		@client.close
	end
end