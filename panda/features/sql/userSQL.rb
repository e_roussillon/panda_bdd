require_relative '../util/dbUtil.rb'

class UserSQL

	def initialize()
		
  	end

  	def clear_table_with_relation()
  		mysql = DB_UTIL.new()
		mysql.query("TRUNCATE TABLE user;")
		mysql.query("TRUNCATE TABLE user_authentication;")
		mysql.query("TRUNCATE TABLE user_filter;")
		mysql.query("TRUNCATE TABLE user_friendship;")
		mysql.query("TRUNCATE TABLE user_notification;")
		mysql.query("TRUNCATE TABLE user_password;")
		mysql.query("TRUNCATE TABLE user_reported;")
		mysql.query("TRUNCATE TABLE user_social;")
		mysql.query("TRUNCATE TABLE user_password;")
		mysql.close
	end

	def insert_default_user(emailField, pseudoField, birthdayField, genderField, passwordField)
		mysql = DB_UTIL.new()
		sql = "INSERT INTO user (`email`, `pseudo`, `birthday`, `gender`, `data_id`) VALUES ( '#{emailField}' , '#{pseudoField}' , '#{birthdayField}' , '#{genderField}' , '1');"
		mysql.query(sql)
		last_id = mysql.last_id

		sql2 = "INSERT INTO user_password (`user_id`, `password`) VALUES ( '#{last_id}' , '#{passwordField}' );"
		mysql.query(sql2)
		mysql.close
	end
end